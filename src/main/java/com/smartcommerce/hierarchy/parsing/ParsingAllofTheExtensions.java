package com.smartcommerce.hierarchy.parsing;

import com.smartcommerce.hierarchy.Search.SearchForExtensionInfoXML;
import com.smartcommerce.hierarchy.buildung.Ex;
import com.smartcommerce.hierarchy.main.Const;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ParsingAllofTheExtensions {

    private SearchForExtensionInfoXML searchForExtensioninfoXML;
    /**
     * all the contextPath or WebRoot for  extensions that do not have a required extension
     */
    private HashMap<String, String> allTheExtensionsThatDoHaveContexPath = new HashMap<>();
    private HashSet<String> allExtensionsAsString = new HashSet<>();
    private HashMap<String, HashSet<String>> allExtensionsWithTheirRequirements = new HashMap<>();
    private HashMap<String, String> extensionAndPath = new HashMap<>();
    private HashMap<String, Ex> extensionsObjectsMap = new HashMap<>();

    public ParsingAllofTheExtensions(SearchForExtensionInfoXML searchForExtensioninfoXML) {
        this.searchForExtensioninfoXML = searchForExtensioninfoXML;
        parsAllOfTheExtensionFiles();
    }

    private void parsAllOfTheExtensionFiles() {


        try {


            for (String paths : searchForExtensioninfoXML.getListOfPaths()) {

                File extensionInfo = new File(paths);

                SAXBuilder saxBuilder = new SAXBuilder();

                Document document = saxBuilder.build(extensionInfo);

                // to get the child node of root element
                Element extensionNode = document.getRootElement().getChild(Const.EXTENSIONNODE);

                // to get the name attribute of the extension
                String name = extensionNode.getAttributeValue(Const.ATTRIBUTENAME);
                // to add all of the extension children in a list
                List<Element> listOfAllChildrenOfExtension = extensionNode.getChildren();


                // THERE is extensions require another extension
                HashSet<String> required = new HashSet<>();

                // to make sure if the extension has  a child node  'REQUIREDEXTENSION'

                if (listOfAllChildrenOfExtension.contains(extensionNode.getChild(Const.CHILDWEBMUDULE))) {


                    Element childOfExtension = extensionNode.getChild(Const.CHILDWEBMUDULE);
                    String webroot = childOfExtension.getAttributeValue(Const.ATTRIBUTEWEBROOT);

                    allTheExtensionsThatDoHaveContexPath.put(name, webroot);
                }


                if (listOfAllChildrenOfExtension.contains(extensionNode.getChild(Const.REQUIREDEXTENSION
                ))) {


                    List<Element> childrenRequirement = extensionNode.getChildren(Const.REQUIREDEXTENSION);
                    for (Element requirement : childrenRequirement) {
                        String nameOfTheRequirement = requirement.getAttributeValue(Const.ATTRIBUTENAME);

                        required.add(nameOfTheRequirement);


                        allExtensionsWithTheirRequirements.put(name, required);

                    }

                    allExtensionsAsString.add(name);
                    extensionAndPath.put(name, paths);
                } else {
                    allExtensionsAsString.add(name);
                    extensionAndPath.put(name, paths);
                }
            }


        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        convertToObject();
        addTheRootExtensions();
        addTheSubExtension();
    }


    private void convertToObject() {
        for (String extension : allExtensionsAsString) {

            Ex ex = new Ex(extension);
            extensionsObjectsMap.put(extension, ex);
        }

    }

    private void addTheRootExtensions() {
        for (Ex extension : extensionsObjectsMap.values()) {

            HashSet<Ex> reqExs = new HashSet<>();
            if (allExtensionsWithTheirRequirements.containsKey(extension.getName()))
                for (String reqExtensions : allExtensionsWithTheirRequirements.get(extension.getName())) {

                    Ex reqEx = extensionsObjectsMap.get(reqExtensions);
                    reqExs.add(reqEx);


                }

            extension.setReqExtensions(reqExs);

        }

    }

    private void addTheSubExtension() {


        for (Ex ex1 : extensionsObjectsMap.values()) {
            HashSet<Ex> subExs = new HashSet<>();

            for (Ex ex2 : extensionsObjectsMap.values()) {
                if (ex2.getReqExtensions().contains(ex1))
                    subExs.add(ex2);

            }


            ex1.setSubExtensions(subExs);

        }

    }


    public HashSet<String> getAllExtensionsAsString() {
        return allExtensionsAsString;
    }


    public HashMap<String, String> getExtensionAndPath() {
        return extensionAndPath;
    }


    public HashMap<String, Ex> getExtensionsObjectsMap() {
        return extensionsObjectsMap;
    }


    public HashMap<String, String> getAllTheExtensionsThatDoHaveContextPath() {
        return allTheExtensionsThatDoHaveContexPath;
    }


}
