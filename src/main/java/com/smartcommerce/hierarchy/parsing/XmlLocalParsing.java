package com.smartcommerce.hierarchy.parsing;


import com.smartcommerce.hierarchy.main.Directory;
import com.smartcommerce.jsonGenerator.main.Const;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class XmlLocalParsing {

    private ArrayList<String> listofExtensions = new ArrayList<>();
    private ArrayList<String> listOfAPI = new ArrayList<>();
    private ArrayList<String> listOfBackoffice = new ArrayList<>();
    private ArrayList<String> listOfAccstorefront = new ArrayList<>();

    private HashMap<String, ArrayList<String>> mapOfAspect = new HashMap<>();

    private Directory directory;


    public XmlLocalParsing(Directory directory) {
        this.directory = directory;
        parseTheLocalExtensions();
    }

    private void parseTheLocalExtensions() {
        try {


            //Create a Document from a file  localextensions
            File localextensions = new File(directory.getPathDirectory() + "/hybris/config/localextensions.xml");

            //Create a DocumentBuilder
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(localextensions);


            // Extract the child from  the root element
            Element classElement = document.getRootElement().getChild(Const.CHILDNODEOFTHEROOTELEMENT);

            //add the children from the extensions node in a list
            List<Element> ListofAllChildrenOfExtensionsNode = classElement.getChildren();


            for (Element childOfExtensions : ListofAllChildrenOfExtensionsNode) {


                String childName = childOfExtensions.getName();
                if (Const.EXTENSION.equals(childName)) {

                    String nameAtr = childOfExtensions.getAttributeValue(Const.ATTRIBUTENAME);
                    String dirAtr = childOfExtensions.getAttributeValue(Const.ATTRIBUTEDIR);
                    String aspect = childOfExtensions.getAttributeValue(Const.ATTRIBUTEASPECT);

                    if (nameAtr != null) {
                        collectExtensions(aspect, nameAtr);

                    } else {
                        if (dirAtr != null) {
                            int lastSlash = dirAtr.lastIndexOf('/') + 1;
                            String newDir = dirAtr.substring(lastSlash);
                            collectExtensions(aspect, newDir);
                        }
                    }
                }
            }


        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }

    }

    private void collectExtensions(String aspect, String newDir) {
        listofExtensions.add(newDir);
        if (aspect != null) {
            switch (aspect) {
                case "api":
                    listOfAPI.add(newDir);
                    mapOfAspect.put("api", listOfAPI);
                    break;
                case "backoffice":
                    listOfBackoffice.add(newDir);
                    mapOfAspect.put("backoffice", listOfBackoffice);
                    break;
                case "accstorefront":
                    listOfAccstorefront.add(newDir);
                    mapOfAspect.put("accstorefront", listOfAccstorefront);
                    break;
            }
        }
    }

    public ArrayList<String> getListOfExtensions() {
        return listofExtensions;
    }

}