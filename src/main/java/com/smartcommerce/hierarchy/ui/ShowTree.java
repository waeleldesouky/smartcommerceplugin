package com.smartcommerce.hierarchy.ui;

import com.smartcommerce.hierarchy.parsing.ParsingAllofTheExtensions;
import com.smartcommerce.hierarchy.parsing.XmlLocalParsing;

public class ShowTree {


    public ShowTree(String extensionName, ParsingAllofTheExtensions parsingAllofTheExtensions, XmlLocalParsing xmlLocalParsing, Type type) {


        MainGraph mainGraph = new MainGraph(extensionName, parsingAllofTheExtensions, xmlLocalParsing, type);
          new MainFrame(mainGraph);

    }
}
