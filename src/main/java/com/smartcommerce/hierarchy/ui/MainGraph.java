package com.smartcommerce.hierarchy.ui;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import com.smartcommerce.hierarchy.buildung.Ex;
import com.smartcommerce.hierarchy.parsing.ParsingAllofTheExtensions;
import com.smartcommerce.hierarchy.parsing.XmlLocalParsing;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.HashSet;

 class MainGraph {
    private ParsingAllofTheExtensions parsingAllofTheExtensions;
    private HashMap<String, Object> cells = new HashMap<>();
    private HashMap<String, Object> edges = new HashMap<>();

    private MainGraph mainGraph;
    private Type type;
    private Boolean duplicated = true;


    private String style = "fillColor=orange";
    private String extensionName;
    private mxGraph graph = new FoldableTree();
    private Object parent = graph.getDefaultParent();
    private XmlLocalParsing xmlLocalParsing;

     MainGraph(String extensionName, ParsingAllofTheExtensions parsingAllofTheExtensions, XmlLocalParsing xmlLocalParsing, Type type) {

        this.xmlLocalParsing = xmlLocalParsing;
        this.extensionName = extensionName;
        this.parsingAllofTheExtensions = parsingAllofTheExtensions;
        this.type = type;
        this.mainGraph = this;
    }


     JComponent getGraph() {

        mxGraphComponent graphComponent = new mxGraphComponent(graph);

        graphComponent.setFoldingEnabled(true);


        graph.getModel().beginUpdate();
        try {
            Object root;
            if (xmlLocalParsing.getListOfExtensions().contains(extensionName)) {

                root = graph.insertVertex(parent, "localRoot", extensionName, 0, 0, 220, 45, style);
            } else root = graph.insertVertex(parent, "localRoot", extensionName, 0, 0, 220, 45);


            if (getDuplicated())
                printHierarchyTree(parsingAllofTheExtensions.getExtensionsObjectsMap().get(extensionName), root);
            else
                printHierarchyTreeND(parsingAllofTheExtensions.getExtensionsObjectsMap().get(extensionName), root);


        } finally {
            graph.getModel().endUpdate();
        }


        mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
        layout.execute(graph.getDefaultParent());
        PopupMenu menu = new PopupMenu(mainGraph, parsingAllofTheExtensions, xmlLocalParsing);
        PopupMenu menu2 = new PopupMenu(mainGraph);

        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    Object cell = graphComponent.getCellAt(e.getX(), e.getY());

                    if (cell != null) {
                        if (!graph.getLabel(cell).equals("")) {
                            String sellectedExtensionName = graph.getLabel(cell);
                            doPop(e, sellectedExtensionName);

                            System.out.println("cell=" + sellectedExtensionName);
                        }
                    } else doChagePop(e);
                }
            }

            private void doChagePop(MouseEvent e) {

                menu2.show(e.getComponent(), e.getX(), e.getY());

            }

            private void doPop(MouseEvent e, String sellectedExtensionName) {
                menu.setExtensionName(sellectedExtensionName);
                menu.show(e.getComponent(), e.getX(), e.getY());
            }

        });


        cells.clear();
        edges.clear();
        return graphComponent;

    }


    private void printHierarchyTree(Ex localRoot, Object root) {
        HashSet<Ex> subordinates;
        if (type == Type.Children) {
            subordinates = localRoot.getSubExtensions();
        } else {
            subordinates = localRoot.getReqExtensions();
        }


        for (Ex e : subordinates) {
            if (xmlLocalParsing.getListOfExtensions().contains(e.getName())) {
                Object v1 = graph.insertVertex(parent, e.getName(), e.getName(), 0, 0, 225, 40, style);

                graph.insertEdge(parent, null, "", root, v1);

                printHierarchyTree(e, v1);
            } else {
                Object v1 = graph.insertVertex(parent, e.getName(), e.getName(), 0, 0, 225, 40);

                graph.insertEdge(parent, null, "", root, v1);

                printHierarchyTree(e, v1);
            }

        }
    }

    private void printHierarchyTreeND(Ex localRoot, Object root) {
        HashSet<Ex> subordinates;
        if (type == Type.Children) {
            subordinates = localRoot.getSubExtensions();
        } else {
            subordinates = localRoot.getReqExtensions();
        }

        for (Ex e : subordinates)
            // to make sure if the cell not already created
            if (!cells.containsKey(e.getName()))
                // if the extension in XmlLocal  , it will be marked .
                if (xmlLocalParsing.getListOfExtensions().contains(e.getName())) {
                    Object v1 = graph.insertVertex(parent, e.getName(), e.getName(), 0, 0, 225, 40, style);
                    checkIfDuplicated(localRoot, root, e, v1);
                } else {
                    Object v1 = graph.insertVertex(parent, e.getName(), e.getName(), 0, 0, 225, 40);
                    checkIfDuplicated(localRoot, root, e, v1);

                }
            else {
                // if the cell is already created, the root will be connected with the it
                if (!edges.containsKey(localRoot.getName() + e.getName())) {
                    Object edge = graph.insertEdge(parent, null, "", root, cells.get(e.getName()));
                    edges.put(localRoot.getName() + e.getName(), edge);
                }
                printHierarchyTreeND(e, cells.get(e.getName()));
            }

    }

    private void checkIfDuplicated(Ex localRoot, Object root, Ex e, Object v1) {
        // to be added in the cells HashMap
        cells.put(e.getName(), v1);
        // to make sure if there is no duplicated edge
        if (!edges.containsKey(localRoot.getName() + e.getName())) {
            // create a new edge
            Object edge = graph.insertEdge(parent, null, "", root, v1);
            edges.put(localRoot.getName() + e.getName(), edge);
        }
        printHierarchyTreeND(e, v1);
    }


    void clear() {
        graph.removeCells(graph.getChildCells(graph.getDefaultParent(), true, true));


    }

    private Boolean getDuplicated() {
        return duplicated;
    }

    void setDuplicated(Boolean duplicated) {
        this.duplicated = duplicated;

    }

    String getExtensionName() {
        return extensionName;
    }


    void setStyle(String style) {
        this.style = style;
    }

    Type getType() {
        return type;
    }
}
