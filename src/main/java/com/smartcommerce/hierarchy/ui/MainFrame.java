package com.smartcommerce.hierarchy.ui;


import javax.swing.*;
import java.awt.*;

  class MainFrame extends JFrame {


      MainFrame(MainGraph mainGraph) {
        super("Hierarchy of the " + mainGraph.getType().toString() + " of " + mainGraph.getExtensionName() + " !");

        this.add(mainGraph.getGraph());
        this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setUndecorated(false);
        this.setVisible(true);

    }

}
