package com.smartcommerce.hierarchy.ui;

import com.smartcommerce.hierarchy.parsing.ParsingAllofTheExtensions;
import com.smartcommerce.hierarchy.parsing.XmlLocalParsing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;

class PopupMenu extends JPopupMenu {
    private MainGraph mainGraph;
    private String extensionName;
    private ParsingAllofTheExtensions parsingAllofTheExtensions;
    private XmlLocalParsing xmlLocalParsing;


    PopupMenu(MainGraph mainGraph, ParsingAllofTheExtensions parsingAllofTheExtensions, XmlLocalParsing xmlLocalParsing) {

        this.mainGraph = mainGraph;
        this.parsingAllofTheExtensions = parsingAllofTheExtensions;
        this.xmlLocalParsing = xmlLocalParsing;
        doPopUpForCells();
    }


    PopupMenu(MainGraph mainGraph) {

        this.mainGraph = mainGraph;
        doPopUpEveryWhere();
    }


    private void doPopUpForCells() {


        JMenuItem getWebrootb = new JMenuItem("get webroot");
        JMenuItem extensionInfo = new JMenuItem("open extension info");
        JMenuItem switchTree;


        if (mainGraph.getType() == Type.Children)
            switchTree = new JMenuItem("get the hierrachy of the required extensions");


        else switchTree = new JMenuItem("get the hierrachy of the subextensions");


        add(getWebrootb);
        add(extensionInfo);
        add(switchTree);

        extensionInfo.addActionListener(e -> {
            String path = parsingAllofTheExtensions.getExtensionAndPath().get(extensionName);
            File file = new File(path);
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.open(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        });


        getWebrootb.addActionListener(e -> {

            if (parsingAllofTheExtensions.getAllTheExtensionsThatDoHaveContextPath().containsKey(extensionName)) {

                String webroot = parsingAllofTheExtensions.getAllTheExtensionsThatDoHaveContextPath().get(extensionName);

                JOptionPane.showMessageDialog(new JFrame(), webroot, "Webroot!!", JOptionPane.INFORMATION_MESSAGE);


            } else
                JOptionPane.showMessageDialog(new JFrame(), extensionName + "  does not have webroot!!", "Webroot!!", JOptionPane.ERROR_MESSAGE);
        });


        switchTree.addActionListener(e -> {

            if (mainGraph.getType() == Type.Children) {
                new ShowTree(extensionName, parsingAllofTheExtensions, xmlLocalParsing, Type.Requirement);
            } else {

                new ShowTree(extensionName, parsingAllofTheExtensions, xmlLocalParsing, Type.Children);
            }

        });

    }


    private void doPopUpEveryWhere() {


        JRadioButtonMenuItem isDub = new JRadioButtonMenuItem("show duplication");
        JRadioButtonMenuItem isNotDub = new JRadioButtonMenuItem("hide duplication");

        isDub.setSelected(true);

        isDub.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {

                mainGraph.clear();

                mainGraph.setDuplicated(true);
                mainGraph.getGraph();

            }
        });
        isNotDub.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                mainGraph.clear();

                mainGraph.setDuplicated(false);
                mainGraph.getGraph();

                //  graph.getGraphComponent().refresh();;
            }
        });
        ButtonGroup group1 = new ButtonGroup();
        group1.add(isDub);
        group1.add(isNotDub);


        JRadioButtonMenuItem marked = new JRadioButtonMenuItem("show marking");
        JRadioButtonMenuItem notMarked = new JRadioButtonMenuItem("hide marking");
        marked.setSelected(true);

        marked.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                mainGraph.clear();

                mainGraph.setStyle("fillColor=orange");

                mainGraph.getGraph();


            }

        });
        notMarked.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                mainGraph.clear();

                mainGraph.setStyle(null);
                mainGraph.getGraph();

            }
        });

        ButtonGroup group2 = new ButtonGroup();
        group2.add(marked);
        group2.add(notMarked);

        add(isDub);
        add(isNotDub);
        addSeparator();

        add(marked);
        add(notMarked);


    }

    void setExtensionName(String extensionName) {
        this.extensionName = extensionName;
    }
}