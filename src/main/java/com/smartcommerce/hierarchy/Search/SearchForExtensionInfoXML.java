package com.smartcommerce.hierarchy.Search;

import com.smartcommerce.hierarchy.main.Const;
import com.smartcommerce.hierarchy.main.Directory;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;


/**
 * this class for search and collect the extensionInfo.xml and add the paths in an arrayList
 */
public class SearchForExtensionInfoXML {
private     Directory directory;
    // every extensionInfo.xml file will be added in this list
    private ArrayList<String> listOfPaths = new ArrayList<>();

    public SearchForExtensionInfoXML(Directory directory) {
        this.directory = directory;
        searchInBinFile();
    }


    /**
     * this method search for all the extensionInfo.xml and put their paths in listOfPaths
     */
    private void searchInBinFile() {


        // here is the directory  to search in it
        File root = new File(directory.getPathDirectory() + "/hybris/bin");

        // the file that we want
        String fileName = Const.EXTENSIONINFOFILE;
        try {
            boolean recursive = true;

            Collection files = FileUtils.listFiles(root, null, recursive);

            for (Object o : files) {
                File file = (File) o;
                if (file.getName().equals(fileName))

                    listOfPaths.add(file.getAbsolutePath());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<String> getListOfPaths() {
        return listOfPaths;
    }


}
