package com.smartcommerce.hierarchy.buildung;

import java.util.HashSet;

public class Ex {
    private String name;

    private HashSet<Ex> reqExtensions = new HashSet<Ex>();
    private HashSet<Ex> subExtensions = new HashSet<Ex>();


    public Ex(String name) {
        this.name = name;
    }

    public HashSet<Ex> getSubExtensions() {
        return subExtensions;
    }

    public void setSubExtensions(HashSet<Ex> subExtensions) {
        this.subExtensions = subExtensions;
    }

    public HashSet<Ex> getReqExtensions() {
        return reqExtensions;
    }

    public void setReqExtensions(HashSet<Ex> reqExtensions) {
        this.reqExtensions = reqExtensions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
