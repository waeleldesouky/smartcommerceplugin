package com.smartcommerce.hierarchy.main;

public interface Const {

    String EXTENSION = "extension";
    String CHILDNODEOFTHEROOTELEMENT = "extensions";
    String EXTENSIONNODE = "extension";
    String ATTRIBUTENAME = "name";
    String ATTRIBUTEDIR = "dir";
    String ATTRIBUTEASPECT = "aspect";
    String CHILDWEBMUDULE = "webmodule";
    String ATTRIBUTEWEBROOT = "webroot";

    String REQUIREDEXTENSION = "requires-extension";
    String EXTENSIONINFOFILE = "extensioninfo.xml";
}

