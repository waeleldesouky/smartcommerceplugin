package com.smartcommerce.hierarchy.main;

import com.smartcommerce.hierarchy.Search.SearchForExtensionInfoXML;
import com.smartcommerce.hierarchy.parsing.ParsingAllofTheExtensions;
import com.smartcommerce.hierarchy.parsing.XmlLocalParsing;
import com.smartcommerce.hierarchy.ui.ShowTree;
import com.smartcommerce.hierarchy.ui.Type;

public class Main {


    public static void main(String[] args) {

      Directory directory = new Directory("/Users/wael/IdeaProjects/SAPCommerce19050");
        SearchForExtensionInfoXML searchForExtensioninfoXML = new SearchForExtensionInfoXML(directory);
        ParsingAllofTheExtensions parsingAllofTheExtensions = new ParsingAllofTheExtensions(searchForExtensioninfoXML);

        XmlLocalParsing xmlLocalParsing = new XmlLocalParsing(directory);
        ShowTree showTree = new ShowTree("backoffice", parsingAllofTheExtensions, xmlLocalParsing, Type.Children);

    }
}