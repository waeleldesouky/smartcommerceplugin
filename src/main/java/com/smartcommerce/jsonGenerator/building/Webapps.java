package com.smartcommerce.jsonGenerator.building;

public class Webapps {

    private String name;
    private String contextPath;

    public Webapps(String name, String contextPath) {
        this.name = name;
        this.contextPath = contextPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
}
