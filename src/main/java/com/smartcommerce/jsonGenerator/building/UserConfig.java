package com.smartcommerce.jsonGenerator.building;

import java.util.ArrayList;

public class UserConfig {


    private ArrayList<ElementOfUserConfig> properties;

    public UserConfig(ArrayList<ElementOfUserConfig> properties) {
        this.properties = properties;
    }

    public ArrayList<ElementOfUserConfig> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<ElementOfUserConfig> properties) {
        this.properties = properties;
    }
}
