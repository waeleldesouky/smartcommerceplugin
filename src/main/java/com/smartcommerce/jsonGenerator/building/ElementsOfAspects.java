package com.smartcommerce.jsonGenerator.building;

import java.util.ArrayList;

public class ElementsOfAspects {

    private String name;
    private ArrayList<Properties> properties;

    private ArrayList<Webapps> webapps;

    public ElementsOfAspects(String name, ArrayList<Webapps> webapps, ArrayList<Properties> properties) {
        this.name = name;
        this.webapps = webapps;
        this.properties = properties;
    }

    public ElementsOfAspects(String name, ArrayList<Webapps> webapps) {
        this.name = name;
        this.webapps = webapps;
    }

    public ArrayList<Properties> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<Properties> properties) {
        this.properties = properties;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Webapps> getWebapps() {
        return webapps;
    }

    public void setWebapps(ArrayList<Webapps> webapps) {
        this.webapps = webapps;
    }
}
