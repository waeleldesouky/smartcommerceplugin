package com.smartcommerce.jsonGenerator.building;

public class Properties {
    private String key;
    private String value;
    private String persona;

    public Properties(String key, String value, String persona) {
        this.key = key;
        this.value = value;
        this.persona = persona;
    }

    public Properties(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }
}
