package com.smartcommerce.jsonGenerator.building;

public class ElementOfUserConfig {


    private String location;
    private String persona;

    public ElementOfUserConfig(String location, String persona) {
        this.location = location;
        this.persona = persona;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }
}
