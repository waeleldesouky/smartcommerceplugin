package com.smartcommerce.jsonGenerator.building;

import java.util.ArrayList;

public class Manifest {

    private String commerceSuiteVersion;
    private boolean useCloudExtensionPack;
    private ArrayList<String> extensions;
    private UserConfig userConfig;

    private ArrayList<ElementsOfAspects> aspects;

    public Manifest(ArrayList<String> extensions, ArrayList<ElementsOfAspects> aspects, UserConfig userConfig) {
        this.extensions = extensions;
        this.userConfig = userConfig;
        this.aspects = aspects;
        this.commerceSuiteVersion = "1905";
        this.useCloudExtensionPack = true;
    }


    public String getCommerceSuiteVersion() {
        return commerceSuiteVersion;
    }

    public void setCommerceSuiteVersion(String commerceSuiteVersion) {
        this.commerceSuiteVersion = commerceSuiteVersion;
    }

    public boolean isUseCloudExtensionPack() {
        return useCloudExtensionPack;
    }

    public void setUseCloudExtensionPack(boolean useCloudExtensionPack) {
        this.useCloudExtensionPack = useCloudExtensionPack;
    }

    public ArrayList<String> getExtensions() {
        return extensions;
    }

    public void setExtensions(ArrayList<String> extensions) {
        this.extensions = extensions;
    }

    public UserConfig getUserConfig() {
        return userConfig;
    }

    public void setUserConfig(UserConfig userConfig) {
        this.userConfig = userConfig;
    }

    public ArrayList<ElementsOfAspects> getAspects() {
        return aspects;
    }

    public void setAspects(ArrayList<ElementsOfAspects> aspects) {
        this.aspects = aspects;
    }
}
