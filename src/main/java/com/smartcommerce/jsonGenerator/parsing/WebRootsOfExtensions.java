package com.smartcommerce.jsonGenerator.parsing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class WebRootsOfExtensions {


    private HashMap<String, ArrayList<String>> mapPaths = new HashMap<>();

    private ExtensionsParsing extensionsParsing;

    public WebRootsOfExtensions(ExtensionsParsing extensionsParsing) {
        this.extensionsParsing = extensionsParsing;
    }

    /**
     * to search for the path and to look if the extension has a requirements or not
     *
     * @param extension
     */
    public void pathFinder(String extension) {


        if (checkIfIthasApath(extension)) {

            ArrayList<String> path = new ArrayList<>();
            path.add(getThePath(extension));
            mapPaths.put(extension, path);


        } else {

            HashSet<String> tree = new HashSet<>();
            allRequiredTree(extension, tree);
            ArrayList<String> paths = new ArrayList<>();

            for (String req : tree) {

                if (checkIfIthasApath(req)) {

                    paths.add(getThePath(req));

                }


            }

            mapPaths.put(extension, paths);


        }


    }

    public ArrayList<String> fetchPaths(String extension) {


        pathFinder(extension);

        return new ArrayList<>(getMapPaths().get(extension));
    }

    /**
     * @param extensionWithRequirments
     * @param tree                     of all of subelements of the extensions with requirements
     */
    public void allRequiredTree(String extensionWithRequirments, HashSet<String> tree) {

        if (hasItReq(extensionWithRequirments)) {

            for (String req : getRequired(extensionWithRequirments)) {
                if (!tree.contains(extensionWithRequirments)) {
                    allRequiredTree(req, tree);
                }
            }

        }

        tree.add(extensionWithRequirments);


    }


    /**
     * @param extension that will be checked if it has a path
     * @return false if it has a required extensions ,true if it has a path
     */
    public boolean checkIfIthasApath(String extension) {


        return extensionsParsing.getAllTheExtensionsThatdoNotRequireAnotherExtensionContexPath().containsKey(extension);

    }

    /**
     * @param extension
     * @return path of the extension
     */
    public String getThePath(String extension) {


        return extensionsParsing.getAllTheExtensionsThatdoNotRequireAnotherExtensionContexPath().get(extension);
    }


    /**
     * @param extension that has requirements
     * @return a list of all required extensions
     */
    public ArrayList<String> getRequired(String extension) {


        return extensionsParsing.getMapOfExceptionWithTheirRequired().get(extension);
    }

    /**
     * @param extension
     * @return checks if it has a requirmetns or not
     */
    public boolean hasItReq(String extension) {

        return extensionsParsing.getMapOfExceptionWithTheirRequired().containsKey(extension);

    }

    public HashMap<String, ArrayList<String>> getMapPaths() {
        return mapPaths;
    }

    public void setMapPaths(HashMap<String, ArrayList<String>> mapPaths) {
        this.mapPaths = mapPaths;
    }
}
