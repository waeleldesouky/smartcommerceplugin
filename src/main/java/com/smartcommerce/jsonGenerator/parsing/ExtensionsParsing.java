package com.smartcommerce.jsonGenerator.parsing;

import com.smartcommerce.jsonGenerator.Search.Search;
import com.smartcommerce.jsonGenerator.main.Const;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExtensionsParsing {

    /**
     * all the contexpath or WebRoot for  extensions that do not have a required extension
     */
    private HashMap<String, String> allTheExtensionsThatdoNotRequireAnotherExtensionContexPath = new HashMap<>();

    /**
     * all the extensions  for  contexpath webroots that do not have a required extension
     */
    private HashMap<String, String> contexPatAallTheExtensionsThatdoNotRequireAnotherExtension = new HashMap<>();


    /**
     * map of the extensions that require another extensions
     */
    private HashMap<String, ArrayList<String>> mapOfExceptionWithTheirRequired = new HashMap<>();

    /**
     * map of all of extensions
     */
    private ArrayList<String> allExtensions = new ArrayList<>();


    private Search search;

    public ExtensionsParsing(Search search) {
        this.search = search;
        parsAllOftheExtensionFiles();
    }


    /**
     * this method to parse all the extensioninfo.xml files
     */

    void parsAllOftheExtensionFiles() {
        try {


            for (String paths : search.getListOfPaths()) {

                File extensioninfo = new File(paths);

                SAXBuilder saxBuilder = new SAXBuilder();

                Document document = saxBuilder.build(extensioninfo);

                // to get the child node of root element
                Element extensionNode = document.getRootElement().getChild(Const.EXTENSIONNODE);

                // to get the name attribute of the extension
                String name = extensionNode.getAttributeValue(Const.ATTRIBUTENAME);
                allExtensions.add(name);
                // to add all of the extension children in a list
                List<Element> listOfallChildrenOfExtension = extensionNode.getChildren();


                // THERE is extensions require another extension
                ArrayList<String> required = new ArrayList<>();

                // to make sure if the extension has  a child node  'webmodule'

                if (listOfallChildrenOfExtension.contains(extensionNode.getChild(Const.CHILDWEBMUDULE))) {


                    Element childOfExtension = extensionNode.getChild(Const.CHILDWEBMUDULE);
                    String path = childOfExtension.getAttributeValue(Const.ATTRIBUTEWEBROOT);

                    allTheExtensionsThatdoNotRequireAnotherExtensionContexPath.put(name, path);
                    contexPatAallTheExtensionsThatdoNotRequireAnotherExtension.put(path, name);
                } else if (listOfallChildrenOfExtension.contains(extensionNode.getChild(Const.REQUIREDEXTENSION
                ))) {


                    List<Element> childrenRequirement = extensionNode.getChildren(Const.REQUIREDEXTENSION);
                    for (Element requirement : childrenRequirement) {
                        String nameOfTheRequirment = requirement.getAttributeValue(Const.ATTRIBUTENAME);

                        required.add(nameOfTheRequirment);
                        mapOfExceptionWithTheirRequired.put(name, required);

                    }


                }

            }


        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> getAllTheExtensionsThatdoNotRequireAnotherExtensionContexPath() {
        return allTheExtensionsThatdoNotRequireAnotherExtensionContexPath;
    }

    public void setAllTheExtensionsThatdoNotRequireAnotherExtensionContexPath(HashMap<String, String> allTheExtensionsThatdoNotRequireAnotherExtensionContexPath) {
        this.allTheExtensionsThatdoNotRequireAnotherExtensionContexPath = allTheExtensionsThatdoNotRequireAnotherExtensionContexPath;
    }

    public HashMap<String, String> getContexPatAallTheExtensionsThatdoNotRequireAnotherExtension() {
        return contexPatAallTheExtensionsThatdoNotRequireAnotherExtension;
    }

    public void setContexPatAallTheExtensionsThatdoNotRequireAnotherExtension(HashMap<String, String> contexPatAallTheExtensionsThatdoNotRequireAnotherExtension) {
        this.contexPatAallTheExtensionsThatdoNotRequireAnotherExtension = contexPatAallTheExtensionsThatdoNotRequireAnotherExtension;
    }

    public HashMap<String, ArrayList<String>> getMapOfExceptionWithTheirRequired() {
        return mapOfExceptionWithTheirRequired;
    }

    public void setMapOfExceptionWithTheirRequired(HashMap<String, ArrayList<String>> mapOfExceptionWithTheirRequired) {
        this.mapOfExceptionWithTheirRequired = mapOfExceptionWithTheirRequired;
    }

    public ArrayList<String> getAllExtensions() {
        return allExtensions;
    }

    public void setAllExtensions(ArrayList<String> allExtensions) {
        this.allExtensions = allExtensions;
    }
}
