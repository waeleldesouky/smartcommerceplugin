package com.smartcommerce.jsonGenerator.Search;

import com.smartcommerce.jsonGenerator.main.Const;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Search {

    private ArrayList<String> listOfPaths = new ArrayList<>();
    private String pathOfSap;

    public Search(String pathOfSap) {
        this.pathOfSap = pathOfSap;
        searchInBinFile();
    }

    /**
     * this method search for all the extensioninfo.xml and put their paths in listOfPaths
     */
    public void searchInBinFile() {


        // here is the directory  to search in it
        File root = new File(pathOfSap + "/hybris/bin");

        // the file that we want
        String fileName = Const.EXTENSIONINFOFILE;
        try {

            Collection files = FileUtils.listFiles(root, null, true);

            for (Iterator iterator = files.iterator(); iterator.hasNext(); ) {
                File file = (File) iterator.next();
                if (file.getName().equals(fileName))

                    listOfPaths.add(file.getAbsolutePath());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public ArrayList<String> getListOfPaths() {
        return listOfPaths;
    }

    public void setListOfPaths(ArrayList<String> listOfPaths) {
        this.listOfPaths = listOfPaths;
    }

    public String getPathOfSap() {
        return pathOfSap;
    }

    public void setPathOfSap(String pathOfSap) {
        this.pathOfSap = pathOfSap;
    }


}
