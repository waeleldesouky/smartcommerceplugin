package com.smartcommerce.jsonGenerator.toJson;

import com.google.gson.Gson;
import com.smartcommerce.jsonGenerator.Search.Search;
import com.smartcommerce.jsonGenerator.building.*;
import com.smartcommerce.jsonGenerator.parsing.ExtensionsParsing;
import com.smartcommerce.jsonGenerator.parsing.WebRootsOfExtensions;
import com.smartcommerce.jsonGenerator.parsing.XmlLocalParsing;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class Json {


    // arrayList that contain all the aspect array elements

    Search search;
    XmlLocalParsing xmlLocalParsing;
    ExtensionsParsing extensionsParsing;
    WebRootsOfExtensions webRootsOfExtensions;
    private ArrayList<ElementsOfAspects> elementsOfAspectsArray = new ArrayList<>();
    private ArrayList<ElementOfUserConfig> ElementOfUserConfigList = new ArrayList<>();


    public Json(Search search, XmlLocalParsing xmlLocalParsing, ExtensionsParsing extensionsParsing, WebRootsOfExtensions webRootsOfExtensions) {
        this.search = search;
        this.xmlLocalParsing = xmlLocalParsing;
        this.extensionsParsing = extensionsParsing;
        this.webRootsOfExtensions = webRootsOfExtensions;
        run();
    }

    public void run() {
        Gson gson = new Gson();


        fillApi();
        fillAccstorefront();
        fillBackoffice();

        // to add user config
        ElementOfUserConfig elementOfUserConfig = new ElementOfUserConfig("add here the location", "add the persona");
        ElementOfUserConfigList.add(elementOfUserConfig);
        UserConfig userConfig = new UserConfig(ElementOfUserConfigList);
//------------------

        ArrayList<String> extensions = xmlLocalParsing.getListofExtensions();

        Manifest manifest = new Manifest(extensions, elementsOfAspectsArray, userConfig);


        // Java objects to File
        try (FileWriter writer = new FileWriter(search.getPathOfSap() + "/generatedManifest.json")) {
            gson.toJson(manifest, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * to fill API
     */

    private void fillApi() {


        // api
//--------------------
        // to add webapp api
        ArrayList<Webapps> webappsListApi = new ArrayList<>();
        // make an Array of the path of the extensions that will be generated to be able to compare the value and prevent the duplications

        ArrayList<String> listOfPath = new ArrayList<>();

        for (String apiExtensionName : xmlLocalParsing.getListOfAPI()) {


            fill(webappsListApi, listOfPath, apiExtensionName);
        }

        // to add properties
        Properties apiProperties = new Properties("xss.filter.header.X-Frame-Options", "");
        Properties apiProperties2 = new Properties("deployment.api.endpoint", "https://api.c3a6o-theofoerc1-d1-public.model-t.cc.commerce.ondemand.com", "development");
        Properties apiProperties3 = new Properties("deployment.api.endpoint", "https://api.c3a6o-theofoerc1-d1-public.model-t.cc.commerce.ondemand.com", "staging");

        ArrayList<Properties> apiPropertiesList = new ArrayList<>();
        apiPropertiesList.add(apiProperties);
        apiPropertiesList.add(apiProperties2);
        apiPropertiesList.add(apiProperties3);

        // to add them to an array Aspect
        ElementsOfAspects apiElementsOfAspects = new ElementsOfAspects("api", webappsListApi, apiPropertiesList);

        // add to Element of Aspect arraylist
        elementsOfAspectsArray.add(apiElementsOfAspects);


    }

    /**
     * to fill  accstorefront
     */

    private void fillAccstorefront() {


        // to add accstorefront
//---------------------
        // to add webapp
        ArrayList<Webapps> webappsListAccstorefront = new ArrayList<>();
        // make an Array of the path of the extensions that will be generated to be able to compare the value and prevent the duplications

        ArrayList<String> listOfPath = new ArrayList<>();


        for (String accstorefrontExtensionName : xmlLocalParsing.getListOfAccstorefront()) {

            fill(webappsListAccstorefront, listOfPath, accstorefrontExtensionName);

        }


        ElementsOfAspects accstorefrontElementsOfAspects = new ElementsOfAspects("accstorefront", webappsListAccstorefront);
        elementsOfAspectsArray.add(accstorefrontElementsOfAspects);


    }


    /**
     * to fill backoffice
     */
    private void fillBackoffice() {
//---------------------
        // to add webapp
        ArrayList<Webapps> webappsListBackoffice = new ArrayList<>();

        // make an Array of the path of the extensions that will be generated to be able to compare the value and prevent the duplications

        ArrayList<String> listOfPath = new ArrayList<>();


        for (String backofficeExtensionName : xmlLocalParsing.getListOfBackoffice()) {

            fill(webappsListBackoffice, listOfPath, backofficeExtensionName);

        }


        ElementsOfAspects accstorefrontElementsOfAspects = new ElementsOfAspects("backoffice", webappsListBackoffice);
        elementsOfAspectsArray.add(accstorefrontElementsOfAspects);


    }

    /**
     * the method is to generate webapp element
     *
     * @param webappsList    to add the webapp object
     * @param listOfWebroots this list to store all the paths // we need it to check if we have dublicated element
     * @param ExtensionName  the extension name
     */

    private void fill(ArrayList<Webapps> webappsList, ArrayList<String> listOfWebroots, String ExtensionName) {
        ArrayList<String> webroots = webRootsOfExtensions.fetchPaths(ExtensionName);
        if (webroots.size() < 2) {
            for (String webroot : webroots) {
                Webapps webapps = new Webapps(ExtensionName, webroot);
                if (!listOfWebroots.contains(webroot)) {
                    listOfWebroots.add(webroot);

                    webappsList.add(webapps);

                }
            }
        } else {
            for (String webroot : webroots) {

                String mainExtension = extensionsParsing.getContexPatAallTheExtensionsThatdoNotRequireAnotherExtension().get(webroot);
                Webapps webapps = new Webapps(mainExtension, webroot);

                if (!listOfWebroots.contains(webroot)) {
                    listOfWebroots.add(webroot);

                    webappsList.add(webapps);

                }
            }


        }

    }
}
