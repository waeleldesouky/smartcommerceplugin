package com.smartcommerce.jsonGenerator.main;

import com.smartcommerce.jsonGenerator.Search.Search;
import com.smartcommerce.jsonGenerator.parsing.ExtensionsParsing;
import com.smartcommerce.jsonGenerator.parsing.WebRootsOfExtensions;
import com.smartcommerce.jsonGenerator.parsing.XmlLocalParsing;
import com.smartcommerce.jsonGenerator.toJson.Json;

public class Main {


    public static void main(String[] args) {


        Search search = new Search("/Users/wael/IdeaProjects/SAPCommerce19050");

        XmlLocalParsing xmlLocalParsing = new XmlLocalParsing(search);
        ExtensionsParsing extensionsParsing = new ExtensionsParsing(search);
        WebRootsOfExtensions webRootsOfExtensions = new WebRootsOfExtensions(extensionsParsing);


        Json json = new Json(search, xmlLocalParsing, extensionsParsing, webRootsOfExtensions);
        json.run();


    }
}
