package subHierarchyPlugin

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage


@State(
        name = "MyDemo",
        storages = [Storage("my-demo.xml")]
)


class MySettings : PersistentStateComponent<MyState> {


    var pluginState = MyState()

    override fun getState(): MyState? {
        return pluginState

    }

    override fun loadState(state: MyState) {


        pluginState = state


    }

    companion object {
        @JvmStatic
        fun getInstance(): PersistentStateComponent<MyState> {

            return ServiceManager.getService(MySettings::class.java)
        }
    }

}