package jsonGeneratorPlugin

import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBLabel
import com.intellij.uiDesigner.core.AbstractLayout
import com.intellij.util.ui.GridBag
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UIUtil
import com.smartcommerce.jsonGenerator.Search.Search
import com.smartcommerce.jsonGenerator.parsing.ExtensionsParsing
import com.smartcommerce.jsonGenerator.parsing.WebRootsOfExtensions
import com.smartcommerce.jsonGenerator.parsing.XmlLocalParsing
import com.smartcommerce.jsonGenerator.toJson.Json
import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

class MyDialogWrapper : DialogWrapper(true) {

    private val panel = JPanel(GridBagLayout())
    private val txtDirectory = JTextField()
    val notGenerated = false;

    init {
        init()
        title = "Json File Generator"
        val state = MySettings.getInstance().state
        if (state != null) {
            txtDirectory.text = state.directory
        }

    }

    override fun createCenterPanel(): JComponent? {

        val gb = GridBag()
                .setDefaultInsets(Insets(0, 0, AbstractLayout.DEFAULT_VGAP, AbstractLayout.DEFAULT_HGAP))
                .setDefaultWeightX(1.0)
                .setDefaultFill(GridBagConstraints.HORIZONTAL)
        panel.preferredSize = Dimension(400, 100)

        panel.add(label("path of the SAP directory "), gb.nextLine().weightx(0.2))
        panel.add(txtDirectory, gb.nextLine().weightx(0.8))

        return panel

    }

    override fun doOKAction() {
        val directory = txtDirectory.text


        val state = MySettings.getInstance().state

        if (!directory.equals(null) && !directory.equals("")) {

            val search = Search(directory)

            val xmlLocalParsing = XmlLocalParsing(search)
            val extensionsParsing = ExtensionsParsing(search)
            val webRootsOfExtensions = WebRootsOfExtensions(extensionsParsing)


            val json = Json(search, xmlLocalParsing, extensionsParsing, webRootsOfExtensions)
            json.run()

            state?.directory = directory
            this.doCancelAction()
        }

    }

    private fun label(text: String): JComponent {

        val label = JBLabel(text)
        label.componentStyle = UIUtil.ComponentStyle.SMALL
        label.fontColor = UIUtil.FontColor.BRIGHTER
        label.border = JBUI.Borders.empty(0, 5, 2, 0)

        return label
    }
}


