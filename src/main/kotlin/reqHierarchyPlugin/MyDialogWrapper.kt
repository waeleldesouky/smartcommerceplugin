package reqHierarchyPlugin

import com.intellij.openapi.ui.DialogWrapper
import com.intellij.ui.components.JBLabel
import com.intellij.uiDesigner.core.AbstractLayout
import com.intellij.util.ui.GridBag
import com.intellij.util.ui.JBUI
import com.intellij.util.ui.UIUtil
import com.smartcommerce.hierarchy.Search.SearchForExtensionInfoXML
import com.smartcommerce.hierarchy.main.Directory
import com.smartcommerce.hierarchy.parsing.ParsingAllofTheExtensions
import com.smartcommerce.hierarchy.parsing.XmlLocalParsing
import com.smartcommerce.hierarchy.ui.ShowTree
import com.smartcommerce.hierarchy.ui.Type
import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.Insets
import javax.swing.*

class MyDialogWrapper : DialogWrapper(true) {

    private val panel = JPanel(GridBagLayout())
    private val txtDirectory = JTextField()
    private val txtExtension = JTextField()


    init {
        init()
        title = "Requirement Hierarchy of an Extension"
         val state = MySettings.getInstance().state
        if (state != null) {
            txtDirectory.text = state.directory
            txtExtension.text = state.extension
        }
    }


    override fun createCenterPanel(): JComponent? {

        val gb = GridBag()
                .setDefaultInsets(Insets(0, 0, AbstractLayout.DEFAULT_VGAP, AbstractLayout.DEFAULT_HGAP))
                .setDefaultWeightX(1.0)
                .setDefaultFill(GridBagConstraints.HORIZONTAL)
        panel.preferredSize = Dimension(400, 100)

        panel.add(label("path of the SAP directory "), gb.nextLine().weightx(0.2))
        panel.add(txtDirectory, gb.nextLine().weightx(0.8))
        panel.add(label("name of the extension"), gb.nextLine().weightx(0.2))
        panel.add(txtExtension, gb.nextLine().weightx(0.8))

        return panel

    }

    override fun doOKAction() {
        val directory = txtDirectory.text
        val extension = txtExtension.text


        val di = Directory(directory)

        val searchForExtensioninfoXML = SearchForExtensionInfoXML(di)
        val parsingAllofTheExtensions = ParsingAllofTheExtensions(searchForExtensioninfoXML)

        val state = MySettings.getInstance().state
val   xmlLocalParsing = XmlLocalParsing (di)




        state?.directory = directory
        state?.extension = extension





        if (parsingAllofTheExtensions.allExtensionsAsString.contains(extension)) {
            val show = ShowTree(extension, parsingAllofTheExtensions,   xmlLocalParsing, Type.Requirement)
             this.doCancelAction()
        } else JOptionPane.showMessageDialog(JFrame(), "the extension is not found ", "Warning!!", JOptionPane.ERROR_MESSAGE)

    }

    private fun label(text: String): JComponent {

        val label = JBLabel(text)
        label.componentStyle = UIUtil.ComponentStyle.SMALL
        label.fontColor = UIUtil.FontColor.BRIGHTER
        label.border = JBUI.Borders.empty(0, 5, 2, 0)

        return label
    }


}
